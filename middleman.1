.TH "MIDDLEMAN" "1" "2023-11-06" "Geek Central" "Miscellaneous"
.
.SH NAME
middleman \(em interception and data logging on TCP connections.
.
.SH SYNOPSIS
Capture mode:
.br
\fBmiddleman\fI [\fB\-\-external\fI] relayspec [relayspec ...]\fR
.br
Analysis mode:
.br
\fBmiddleman\fI extractionspec [extractionspec ...] [logfile]\fR
.
.SH DESCRIPTION
In its \fIcapture\fR mode,
.B middleman
listens on one or more specified TCP ports, and relays any connections
on each one to a specified corresponding destination address. It also
logs all data passed on the incoming connection in either direction to
standard output, prefixing each line with a timestamp and indication
of source/destination addresses.
.
.P
.B middleman
handles both IPv4 and IPv6; the mode of each connection is chosen
based on the form of the address to relay connections to.
.
.P
Other tools like \fBsocat\fR and \fBnetcat\fR can perform the relaying
function, as well as handling more general types of connections, but
none of them seems to offer the logging ability, perhaps because this
cannot be implemented in any sensible fashion for general connection
types. By concentrating exclusively on TCP connections,
\fBmiddleman\fR can provide this feature in a straightforward way.
.
.P
.B middleman
also has the ability to analyze sequences of its logging output that have
been saved to a file. In this \fIanalysis mode\fR, it can be passed a
whole variety of criterion specifications, to choose which lines of logging
to display.
.
.P
The mode of operation is decided by the command-line options that are
passed: the presence of one or more \fIrelayspec\fRs (i.e.
\fB\-\-relay\fR options) indicates capture mode, while the presence of
one or more \fIextractionspec\fRs (i.e. \fB\-\-extract\fR options)
indicates analysis mode. The two cannot be mixed.
.
.SH OPTIONS
.
.TP
\fB\-\-external\fR
(capture mode only) indicates that listening sockets are to be opened
on the \f[CW]0.0.0.0\fR or \f[CW][::]\fR address, allowing connections from other network
nodes. Otherwise, they are opened on the \f[CW]127.0.0.1\fR or \f[CW][::1]\fR (loopback) address,
which means connections can only come from the same network node.
.
.TP
\fB\-\-extract=\fIcriterion[\fB:\fIcriterion ...]\fR
indicates analysis mode; \fBmiddleman\fR will read the \fIlogfile\fR or,
if that is not specified, its standard input, expecting it to be formatted
in the same way as output generated in
capture mode, and will select lines for display according to the various
\fIcriterion\fR settings.
.IP
Each \fIcriterion\fR takes one of the forms:
.
.RS 7
.TP
\fBsincetime=\fItime\fR
The timestamp on the line must be at least \fItime\fR in UTC seconds.
.TP
\fBbeforetime=\fItime\fR
The timestamp on the line must be less then \fItime\fR in UTC seconds.
.TP
\fBlocalport=\fIport\fR
\fBmiddleman\fR’s listening port number must equal \fIport\fR.
.TP
\fBremoteaddr=\fIaddr\fR
The address of the peer that connected to \fBmiddleman\fR must equal \fIaddr\fR.
.TP
\fBremoteport=\fIport\fR
The port number of the connection from the peer must equal \fIport\fR.
.TP
\fBdirection=\fI[\fBout\fI|\fBin\fI]\fR
The direction in which the data passed must match that given: “\fBout\fR”
for outgoing data (from the remote to \fBmiddleman\fR and thence to the peer),
“\fBin\fR" for incoming data (from the peer to \fBmiddleman\fR, and thence
to the remote).
.RE
.
.IP
Multiple \fIcriterion\fR specs are interpreted as a logical-and of
the conditions: the extraction spec is only satisfied if \fIall\fR
criteria it includes are satisfied.
.
.IP
This option may be repeated multiple times; log lines will be displayed
if they match at least one of the extraction specs (logical-or of multiple
extraction specs).
.
.TP
\fB\-\-relay=\fIlocalport\fB:\fIremoteaddr\fB:\fIremoteport\fR
indicates capture mode: \fBmiddleman\fR will listen on the port numbered
\fIlocalport\fR; any incoming connection
on this port will trigger a corresponding outgoing connection to port
\fIremoteport\fR at address \fIremoteaddr\fR (host name or IP address), after which all data
received at one end will be passed to the other end.
.
.IP
This option may be repeated multiple times, to allow relaying
of connections to/from multiple addresses/ports at once.
.
.SH OUTPUT FORMAT
Each line of output looks like this:
.RS 4
«timestamp»:«localport»/«remoteaddr»:«remoteport»«code»«data»
.RE
where
.IP «timestamp»
is the UTC timestamp in seconds, to 6 decimal places.
.IP «localport»
is the local port number on which \fBmiddleman\fR accepted the incoming connection.
.IP «remoteaddr»
is the address of the client which made the connection to \fBmiddleman\fR.
.IP «remoteport»
is the port number on which the client connected.
.IP «code»
is a single-character code indicating the activity being logged:
.RS 7
.IP “<”
data received on the connection
.IP “>”
data sent on the connection
.IP “!”
diagnostic message, e.g. incoming connection accepted, I/O error or connection closing.
.RE
.IP «data»
is the data which was sent or received on the connection, in quoted-printable
encoding, or explanatory text for a diagnostic message.
.
.SH EXAMPLES
.
.P
.RS 4
middleman \-\-relay=8084:10.0.3.177:80 | tee sniff.log
.RE
.P
accepts connections on local port 8084 and relays them to port 80 at
the address 10.0.3.177; traffic on these connections will be written
to standard output and also saved to the file \f[CW]sniff.log\fR.
.
.P
.RS 4
middleman \-\-extract=sincetime=$(date \-d "\-10 minutes" +%s):remoteaddr=10.0.3.162 sniff.log
.RE
.P
extracts and displays entries from \f[CW]sniff.log\fR logging data
sent to or received from connections from the address 10.0.3.162
starting from at most 10 minutes ago.
